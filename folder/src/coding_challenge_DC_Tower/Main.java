package coding_challenge_DC_Tower;

public class Main {

	public static void main(String[] args) {

		System.out.println("*************AUFZUEGE ERSTELLEN**************");
		Aufzug.AufzugErstellung(Aufzug.getLiftanzahl());
		System.out.println("*************AUFZUG STATUS**************");
		Aufzug.AufzugStatus();
		System.out.println("*************VERFUEGBARKEIT**************");
		Aufzug.VerfuegbareAufzuegeAnzeigen();
		System.out.println("*************ANFRAGEN**************");
		Anfrage.AnfrageErstellung(4);
		Anfrage.AnfrageErstellen(0, 12);
		Anfrage.AnfrageErstellen(31, 0);
		System.out.println("*************ANFRAGE STATUS**************");
		Anfrage.AnfrageStatus();

	}

}
