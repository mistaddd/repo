package coding_challenge_DC_Tower;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

//Aufzug Klasse um Aufzug Objekte zu erstellen. Diese haben Begrenzungen (siehe statische Variablen), 
//eine Nummer, eine aktuelle Position, eine Ziel Position und eine Fahrtrichtung.

public class Aufzug {

	// Statische Variablen fuer Begrenzungen (z.B Anzahl, MinPosition, MaxPosition,
	// usw.) sowie statische Arrayliste aller Liftobjekte.

	private static final int LIFTANZAHL = 7;
	private static final int MINPOSITION = 0;
	private static final int MAXPOSITION = 55;
	public static ArrayList<Aufzug> AUFZUGLISTE = new ArrayList<>();

	// Variablen von Aufzugobjekten

	private int liftNummer;
	private int aktuellePosition;
	private int zielPosition;
	private Richtung fahrtRichtung;

	// Constructor

	public Aufzug(int liftNummer, int aktuellePosition, int zielPosition, Richtung fahrtRichtung) {
		this.liftNummer = liftNummer;
		this.aktuellePosition = aktuellePosition;
		this.fahrtRichtung = fahrtRichtung;
		this.zielPosition = zielPosition;

	}

	// Methoden

	// AufzugErstellung: Durch Uebergabe des anzahl Parameter wird die Anzahl der zu
	// erstellenden Lifte festgelegt. Es werden randomisierte Lifte erstellt.

	public static void AufzugErstellung(int anzahl) {

		for (int i = 1; i <= anzahl; i++) {

			int aktuellePosition = ThreadLocalRandom.current().nextInt(Aufzug.getMinposition(), 1 + 1);

			if (aktuellePosition == 1) {
				aktuellePosition = ThreadLocalRandom.current().nextInt(Aufzug.getMinposition() + 1,
						Aufzug.getMaxposition() + 1);
			}

			int zielPosition = 0;
			Richtung richtung;

			if (aktuellePosition == 0) {
				if (ThreadLocalRandom.current().nextInt(1, 2 + 1) == 1) {
					richtung = Richtung.STEHT;
					zielPosition = 0;
				} else {
					richtung = Richtung.RAUF;
					zielPosition = ThreadLocalRandom.current().nextInt(Aufzug.getMinposition() + 1,
							Aufzug.getMaxposition() + 1);
				}

			} else {
				richtung = Richtung.getRandomRichtung(1);

				if (richtung == Richtung.RAUF) {
					zielPosition = ThreadLocalRandom.current().nextInt(aktuellePosition + 1,
							Aufzug.getMaxposition() + 1);
				} else if (richtung == Richtung.RUNTER) {
					zielPosition = ThreadLocalRandom.current().nextInt(Aufzug.getMinposition(), aktuellePosition);
				}

			}

			getAUFZUGLISTE().add(new Aufzug(i, aktuellePosition, zielPosition, richtung));
		}
	}

	// ZielDistanzAusgeben: Gibt die Distanz zwischen der Zielposition und der
	// aktuellenPosition eines Liftes aus.

	public int ZielDistanzAusgeben() {
		int distanz = 0;

		if (this.aktuellePosition > this.zielPosition) {
			distanz = this.aktuellePosition - this.zielPosition;
		} else if (this.aktuellePosition < this.zielPosition) {
			distanz = this.zielPosition - this.aktuellePosition;
		}

		return distanz;
	}

	// Aufzugstatus: Gibt den Status aller erstellten Lifte aus.

	public static void AufzugStatus() {

		for (Aufzug aufzug : getAUFZUGLISTE()) {
			System.out.println(aufzug.toString());

		}

	}

	// VerfuegbareAufzuegeAnzeigen: Gibt Aufzuege aus die den nicht in Bewegung
	// sind. (die also STEHEN)

	public static void VerfuegbareAufzuegeAnzeigen() {

		for (Aufzug aufzug : AUFZUGLISTE) {

			if (aufzug.getFahrtRichtung() == Richtung.STEHT) {
				System.out.println(aufzug + " ist verfuegbar.");
			}

		}

	}

	//toString
	
	@Override
	public String toString() {
		return "[liftNummer=" + liftNummer + ", aktuellePosition=" + aktuellePosition + ", zielPosition=" + zielPosition
				+ ", fahrtRichtung=" + fahrtRichtung + "]";
	}

	//Getter & Setter
	
	public static ArrayList<Aufzug> getAUFZUGLISTE() {
		return AUFZUGLISTE;
	}

	public static void setAUFZUGLISTE(ArrayList<Aufzug> aUFZUGLISTE) {
		AUFZUGLISTE = aUFZUGLISTE;
	}

	public int getAktuellePosition() {
		return aktuellePosition;
	}

	public void setAktuellePosition(int aktuellePosition) {
		this.aktuellePosition = aktuellePosition;
	}

	public Richtung getFahrtRichtung() {
		return fahrtRichtung;
	}

	public void setFahrtRichtung(Richtung fahrtRichtung) {
		this.fahrtRichtung = fahrtRichtung;
	}

	public int getLiftNummer() {
		return liftNummer;
	}

	public static int getLiftanzahl() {
		return LIFTANZAHL;
	}

	public static int getMinposition() {
		return MINPOSITION;
	}

	public static int getMaxposition() {
		return MAXPOSITION;
	}

	public int getZielPosition() {
		return zielPosition;
	}

	public void setZielPosition(int zielPosition) {
		this.zielPosition = zielPosition;
	}

}
