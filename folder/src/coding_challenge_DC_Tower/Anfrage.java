package coding_challenge_DC_Tower;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

//Anfrage Klasse um Anfrage Objekte zu erstellen. Diese haben Begrenzungen (siehe statische Variablen), 
//eine AnfrageNummer, eine Stockwerk, eine Ziel, eine Zielrichtung und einen Wahlaufzug.

public class Anfrage {

	// Statische Variablen fuer Begrenzungen wie MaxAnfragen sowie statische
	// Arraylisten aller AnfrageObjekte.

	public static final int MAXANFRAGEN = 7;
	public static ArrayList<Anfrage> ANFRAGELISTE = new ArrayList<Anfrage>();
	public static ArrayList<Aufzug> AUFZUGWAHLLISTE = Aufzug.getAUFZUGLISTE();

	// Variablen von Anfrageobjekten

	private int anfrageNummer;
	private int stockwerk;
	private int ziel;
	private Richtung zielRichtung;
	private Aufzug wahlAufzug;

	// Constructor

	public Anfrage(int anfrageNummer, Aufzug wahlAufzug, int stockwerk, int ziel, Richtung richtung) {
		this.anfrageNummer = anfrageNummer;
		this.stockwerk = stockwerk;
		this.ziel = ziel;
		this.zielRichtung = richtung;
		this.wahlAufzug = wahlAufzug;
	}

	// METHODEN

	// Anfragestatus: Gibt den Status aller erstellten Anfragen aus.

	public static void AnfrageStatus() {

		try {
			for (Anfrage anfrage : getANFRAGELISTE()) {
				System.out.println(anfrage.toString());

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// WahlListeErneuern: Methode um die Liste der Wahllifte zu erneuern.

	public static void WahlListeErneuern() {

		try {
			setAUFZUGWAHLLISTE(Aufzug.getAUFZUGLISTE());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// AnfrageErstellen: Durch Uebergabe der stockwerk und ziel Parameter wird eine
	// Anfrage erstellt.

	public static void AnfrageErstellen(int stockwerk, int ziel) {

		int anfrageNummer = 0;
		int vorhandeneAnfragen = getANFRAGELISTE().size();

		try {
			if (vorhandeneAnfragen < MAXANFRAGEN) {
				anfrageNummer = vorhandeneAnfragen + 1;

				Richtung richtung;

				if (stockwerk == 0) {

					richtung = Richtung.RAUF;
				} else {

					richtung = Richtung.RUNTER;
				}

				getANFRAGELISTE().add(new Anfrage(anfrageNummer, AufzugFinden(stockwerk), stockwerk, ziel, richtung));

			} else {

				System.out.println("Anfragemaximum erreicht. Bitte warten Sie einen Augenblick.");

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// AnfrageErstellung: Durch Uebergabe des anzahl Parameter wird die Anzahl der
	// zu
	// erstellenden Anfragen festgelegt. Es werden randomisierte Anfragen erstellt.

	public static void AnfrageErstellung(int anzahl) {

		try {
			for (int i = 1; i <= anzahl; i++) {

				int aktuellePosition = ThreadLocalRandom.current().nextInt(Aufzug.getMinposition(),
						Aufzug.getMaxposition() + 1);
				int zielPosition;
				Richtung richtung;

				if (aktuellePosition == 0) {
					zielPosition = ThreadLocalRandom.current().nextInt(Aufzug.getMinposition(),
							Aufzug.getMaxposition() + 1);
					richtung = Richtung.RAUF;
				} else {
					zielPosition = 0;
					richtung = Richtung.RUNTER;
				}

				AnfrageErstellen(aktuellePosition, zielPosition);
			}

			WahlListeErneuern();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Aufzugfinden: Mehtode um den am besten geeigneten Lift fuer die Anfrage zu
	// finden.

	public static Aufzug AufzugFinden(int aktuellePosition) {

		ArrayList<Aufzug> aufzugListe = getAUFZUGWAHLLISTE();
		Aufzug freierAufzug = null;
		int distanz = Aufzug.getMaxposition();

		for (Aufzug aufzug : aufzugListe) {
			if (aufzug.getFahrtRichtung().equals(Richtung.STEHT)) {
				freierAufzug = aufzug;
				getAUFZUGWAHLLISTE().remove(freierAufzug);
				return freierAufzug;
			}
		}

		if (freierAufzug == null) {
			for (Aufzug aufzug : aufzugListe) {
				if (aufzug.getAktuellePosition() > aktuellePosition && aufzug.getFahrtRichtung() == Richtung.RUNTER) {

					if (aufzug.getAktuellePosition() - aktuellePosition < distanz) {
						freierAufzug = aufzug;
						distanz = aufzug.getAktuellePosition() - aktuellePosition + aufzug.ZielDistanzAusgeben();

					}

				} else if (aufzug.getAktuellePosition() < aktuellePosition
						&& aufzug.getFahrtRichtung() == Richtung.RAUF) {

					if (aktuellePosition - aufzug.getAktuellePosition() < distanz) {
						freierAufzug = aufzug;
						distanz = aufzug.getAktuellePosition() - aktuellePosition + aufzug.ZielDistanzAusgeben();

					}

				} else {

					if (aufzug.getAktuellePosition() < aktuellePosition) {
						if (aktuellePosition - aufzug.getAktuellePosition() < distanz) {
							freierAufzug = aufzug;
							distanz = aufzug.getAktuellePosition() - aktuellePosition + aufzug.ZielDistanzAusgeben();

						}
					} else if (aufzug.getAktuellePosition() > aktuellePosition) {

						if (aufzug.getAktuellePosition() - aktuellePosition < distanz) {
							freierAufzug = aufzug;
							distanz = aufzug.getAktuellePosition() - aktuellePosition + aufzug.ZielDistanzAusgeben();

						}
					}

				}
			}

		}
		aufzugListe.remove(freierAufzug);
		return freierAufzug;
	}

	// toString

	@Override
	public String toString() {
		return "[anfrageNummer=" + anfrageNummer + ", stockwerk=" + stockwerk + ", ziel=" + ziel + ", zielRichtung="
				+ zielRichtung + ", wahlAufzug=" + wahlAufzug + "]";
	}

	// Getter & Setter

	public Aufzug getAufzug() {
		return wahlAufzug;
	}

	public void setAufzug(Aufzug aufzug) {
		this.wahlAufzug = aufzug;
	}

	public int getStockwerk() {
		return stockwerk;
	}

	public void setStockwerk(int stockwerk) {
		this.stockwerk = stockwerk;
	}

	public int getZielPosition() {
		return ziel;
	}

	public void setZielPosition(int zielPosition) {
		this.ziel = zielPosition;
	}

	public static ArrayList<Anfrage> getANFRAGELISTE() {
		return ANFRAGELISTE;
	}

	public static void setANFRAGELISTE(ArrayList<Anfrage> aNFRAGELISTE) {
		ANFRAGELISTE = aNFRAGELISTE;
	}

	public int getAnfrageNummer() {
		return anfrageNummer;
	}

	public static ArrayList<Aufzug> getAUFZUGWAHLLISTE() {
		return AUFZUGWAHLLISTE;
	}

	public static void setAUFZUGWAHLLISTE(ArrayList<Aufzug> aUFZUGWAHLLISTE) {
		AUFZUGWAHLLISTE = aUFZUGWAHLLISTE;
	}

	public Richtung getZielRichtung() {
		return zielRichtung;
	}

	public void setZielRichtung(Richtung zielRichtung) {
		this.zielRichtung = zielRichtung;
	}

	public Aufzug getWahlAufzug() {
		return wahlAufzug;
	}

	public void setWahlAufzug(Aufzug wahlAufzug) {
		this.wahlAufzug = wahlAufzug;
	}

	public static int getMaxanfragen() {
		return MAXANFRAGEN;
	}

}
