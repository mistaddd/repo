package coding_challenge_DC_Tower;

import java.util.Random;

public enum Richtung {

	RUNTER, RAUF, STEHT;

	public static Richtung getRandomRichtung(int laenge) {
		Random random = new Random();
		return values()[random.nextInt(values().length - laenge)];
	}

}
